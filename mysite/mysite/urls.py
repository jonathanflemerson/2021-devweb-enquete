"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))

    MEUS COMENTARIOS
     1. Caso não aja mais informações, automaticamente será lido as urls de main.
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [

    path('',include('main.urls')),
    path('enquetes/',include('enquetes.urls')),
    path('financeapp/',include('financeapp.urls')),
    path('admin/', admin.site.urls),
]
