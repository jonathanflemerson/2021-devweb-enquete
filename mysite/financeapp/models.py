from django.db import models
from django.db.models import Max

# Create your models here.
class Usuario(models.Model):
    nome = models.CharField(max_length = 100)
    def __str__(self):
        return self.nome

class Receita(models.Model):
    servico = models.CharField(max_length = 100)
    salario = models.FloatField(default = 0)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    def __str__(self):
        return self.servico


class DespesaFixa(models.Model):
    despesa = models.CharField(max_length = 100)
    valor = models.FloatField(default = 0)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    def __str__(self):
        return self.despesa

class DespesaEventual(models.Model):
    despesa = models.CharField(max_length = 100)
    valor = models.FloatField(default = 0)
    tempo_divida_mes = models.IntegerField(default = 0)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    def __str__(self):
        return self.despesa





