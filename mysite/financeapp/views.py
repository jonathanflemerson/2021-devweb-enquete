from django.shortcuts import render, get_object_or_404, Http404
from django.http import HttpResponseRedirect
from .models import Usuario, Receita, DespesaFixa,DespesaEventual
from django.urls import reverse
from django.views import generic
from django.utils import timezone
# Create your views here.
'''
def index(resquest):
    resposta = "<h2>Finance App</h2>"
    return HttpResponse(resposta)
'''

class IndexView(generic.View):
    def get(self, request,*args,**kargs):
        usuarios = Usuario.objects.all()
        contexto = { 'usuarios': usuarios }
        return render(request, 'financeapp/index.html', contexto)

class DetalhesView(generic.View):
    def get(self, request, *args, **kwargs):
        usuario_id = self.kwargs['pk']
        usuario = get_object_or_404(Usuario, pk = usuario_id)
        return render(request, 'financeapp/usuario_detail.html', {'usuario':usuario})
