# Generated by Django 2.2.7 on 2021-02-11 02:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('financeapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='despesaeventual',
            name='valor',
            field=models.FloatField(default=0),
        ),
    ]
