from django.contrib import admin
from .models import Usuario, Receita, DespesaFixa, DespesaEventual
# Register your models here.

class ReceitaInline(admin.TabularInline):
    model = Receita
    extra = 2

class DespesaFixaInline(admin.TabularInline):
    model = DespesaFixa
    extra = 2

class DespesaEventualInline(admin.TabularInline):
    model = DespesaEventual
    extra = 2



class UsuarioAdmin(admin.ModelAdmin):
    fieldsets = [
            ('Nome',{'fields': ['nome']}),
        ]
    inlines = [ReceitaInline, DespesaFixaInline, DespesaEventualInline]
    list_display = ('nome','id')
    search_fields = ['nome']

admin.site.register(Usuario, UsuarioAdmin)