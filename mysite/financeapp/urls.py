from django.urls import path
from . import views

app_name = 'financeapp'
urlpatterns = [
    path('',views.IndexView.as_view(),name="index"),
    path('<int:pk>/', views.DetalhesView.as_view(), name="detalhes"),
]