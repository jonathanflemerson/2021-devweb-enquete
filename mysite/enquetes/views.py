from django.shortcuts import render, get_object_or_404, Http404
from django.http import HttpResponseRedirect
from .models import Pergunta, Opcao
from django.urls import reverse
from django.views import generic
from django.utils import timezone

# Create your views here.




class IndexView(generic.View):
    def get(self, request,*args,**kargs):
        ultimas_enquetes = Pergunta.objects.filter(
            data_publicacao__lte = timezone.now()
            ).order_by('-data_publicacao')[:5]
        contexto = { 'ultimas_enquetes': ultimas_enquetes }
        return render(request, 'enquetes/index.html', contexto)

class DetalhesView(generic.View):
    def get(self, request, *args, **kwargs):
        enquete_id = self.kwargs['pk']
        enquete = get_object_or_404(Pergunta, pk = enquete_id)
        agora = timezone.now()
        if enquete.data_publicacao > agora:
           raise Http404('Enquete inexistente')
        return render(request, 'enquetes/pergunta_detail.html', {'pergunta':enquete})


class ResultadoView(generic.View):
    def get(self, request, *args, **kwargs):
        enquete_id = self.kwargs['pk']
        enquete = get_object_or_404(Pergunta, pk =enquete_id)
        return render(request, 'enquetes/resultado.html', {'enquete':enquete})

class VotacaoView(generic.View):
    def post(self, request, *args, **kwargs):
        enquete_id = self.kwargs['enquete_id']
        enquete = get_object_or_404(Pergunta, pk=enquete_id)
        try:
            op_desejada = enquete.opcao_set.get(pk=request.POST['opcao'])
        except (KeyError, Opcao.DoesNotExist):
            contexto = {
                        'enquete':enquete,
                        'error_message': "Deve selecionar antes uma opção valida",
                        }
            return render(request, 'enquetes/detalhes.html/', contexto)
        else:
            op_desejada.votos += 1.0
            op_desejada.save()
            return HttpResponseRedirect(reverse('enquetes:resultado', args = (enquete.id,)))


'''
##### Outras opções para os elementos de views #######
'''

'''
def index(request):
    ultimas_enquetes = Pergunta.objects.order_by('-data_publicacao')[:5]
    contexto = { 'ultimas_enquetes': ultimas_enquetes }
    return render(request, 'enquetes/index.html', contexto)


class IndexView(generic.ListView):
    template_name = 'enquetes/index.html'
    context_object_name = 'ultimas_enquetes'
    def get_queryset(self):
        return Pergunta.objects.order_by('-data_publicacao')[:5]
'''

'''
def detalhes(request, enquete_id):
    enquete = get_object_or_404(Pergunta, pk = enquete_id)
    return render(request, 'enquetes/detalhes.html', {'enquete':enquete})
'''
'''
class DetalhesView(generic.DetailView):
      model = Pergunta
      context_object_name = 'enquete'
      template_name = 'enquetes/detalhes.html'


class DetalhesView(generic.DetailView):
    model = Pergunta
'''

'''
def resultados(request, enquete_id):
    enquete = get_object_or_404(Pergunta, pk = enquete_id)
    return render(request, 'enquetes/resultado.html', {'enquete':enquete})


class ResultadoView(generic.DetailView):
      model = Pergunta
      context_object_name = 'enquete'
      template_name = 'enquetes/resultado.html'
'''
'''
    def votacao(request, enquete_id):
        enquete = get_object_or_404(Pergunta, pk = enquete_id)
        try:
            op_desejada = enquete.opcao_set.get(pk=request.POST['opcao'])
        except (KeyError, Opcao.DoesNotExist):
            contexto = {
                        'enquete':enquete,
                        'error_message': "Deve selecionar antes uma opção valida",
                        }
            return render(request, 'enquetes/detalhes.html/', contexto)
        else:
            op_desejada.votos += 1.0
            op_desejada.save()
            return HttpResponseRedirect(reverse('enquetes:resultado', args = (enquete.id,)))
'''