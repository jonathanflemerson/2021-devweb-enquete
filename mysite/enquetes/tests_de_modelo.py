from django.test import TestCase
from django.utils import timezone
from .models import Pergunta
from django.urls import reverse
import datetime


# Create your tests here.

class PerguntaTest(TestCase):
    def test_publicada_recentemente_pergunta_futura(self):
        '''
        O metodo publicada_recentemente deve devolver false quando se trata de
        publicacções no futuro.
        '''
        data_teste = timezone.now() + datetime.timedelta(days=30)
        pergunta = Pergunta(data_publicacao = data_teste)
        self.assertIs(pergunta.publicada_recentemente(), False)

    def test_publicada_recentemente_pergunta_abaixo_24hrs(self):
       '''
       O metodo publicada_recentemente deve devolver false quando se trata de
        publicacções abaixo de 24hrs.
       '''
       data_teste = timezone.now() - datetime.timedelta(days=1, seconds =1)
       pergunta = Pergunta(data_publicacao = data_teste)
       self.assertIs(pergunta.publicada_recentemente(), False)

    def test_publicada_recentemente_pergunta_dentro_24hrs(self):
       '''
       O metodo publicada_recentemente deve devolver true quando se trata de
        publicacções dentro de 24hrs.
       '''
       data_teste = timezone.now() - datetime.timedelta(hours=23,minutes=59,seconds =59)
       pergunta = Pergunta(data_publicacao = data_teste)
       self.assertIs(pergunta.publicada_recentemente(), True)
