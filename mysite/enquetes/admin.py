from django.contrib import admin
from .models import Pergunta, Opcao, Rotulo, Autor, Perfil

# Register your models here.

class OpcaoInline(admin.TabularInline):
    model = Opcao
    extra = 2

class AutorInline(admin.StackedInline):
    model = Autor


class PerguntaAdmin(admin.ModelAdmin):
    fieldsets = [
            (None,{'fields': ['texto','rotulos']}),
            ('Informação do autor',{'fields': ['autor']}),
            ('Informação de data',{'fields': ['data_publicacao']}),
        ]
    inlines = [OpcaoInline]
    list_display = ('texto','id','data_publicacao','publicada_recentemente')
    search_fields = ['texto']
    list_filter = ['data_publicacao']

class PerfilAdmin(admin.ModelAdmin):
    inlines = [AutorInline]

admin.site.register(Perfil, PerfilAdmin)
admin.site.register(Rotulo)
admin.site.register(Pergunta, PerguntaAdmin)


admin.AdminSite.site_header = 'Aplicação de enquetes - Administração'
admin.AdminSite.index_title = 'Administração das Aplicações'
