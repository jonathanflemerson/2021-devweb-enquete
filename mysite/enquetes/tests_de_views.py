from django.test import TestCase
from django.utils import timezone
from .models import Pergunta
from django.urls import reverse
import datetime


# Create your tests here.
def cria_enquete(texto, quant_dias):
    '''
    Cria um objeto do tipo Pergunta, com um texto e uma data representada por uma quantidade
    de dias, que pode ser positiva ou negativa, e salva no banco de testes.
    '''
    data_teste = timezone.now() + datetime.timedelta(days=quant_dias)
    return Pergunta.objects.create(texto=texto, data_publicacao = data_teste)


class IndexViewTest(TestCase):
    def test_indexview_sem_perguntas(self):
        '''
        Se não existirem enquetes cadastradas será exibido mensagem apropriada.
        '''
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Nenhuma pergunta cadastrada.")
        self.assertQuerysetEqual(resposta.context['ultimas_enquetes'], [])

    def test_indexview_com_enquete_no_passado(self):
        '''
        enquetes com data de publicação no passado são apresentadas.
        '''
        cria_enquete(texto='enquete no passado',quant_dias=-30)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "enquete no passado")
        self.assertQuerysetEqual(resposta.context['ultimas_enquetes'], ['<Pergunta: enquete no passado>'])

    def test_indexview_com_enquete_no_futuro(self):
        '''
        enquetes no futuro NÂO são exibidas.
        '''
        cria_enquete(texto='enquete no passado',quant_dias=30)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Nenhuma pergunta cadastrada.")
        self.assertQuerysetEqual(resposta.context['ultimas_enquetes'], [])

    def test_indexview_com_enquete_no_passado_e_outra_no_futuro(self):
        '''
        Apenas a enquete publicada no passado é exibida.
        '''
        cria_enquete(texto='enquete no futuro',quant_dias=3)
        cria_enquete(texto='enquete no passado',quant_dias=-3)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "enquete no passado")
        self.assertQuerysetEqual(resposta.context['ultimas_enquetes'], ['<Pergunta: enquete no passado>'])

    def test_indexview_com_duas_enquetes_no_passado(self):
        '''
        As duas enquetes no passado são exibidas.
        '''
        cria_enquete(texto='enquete no passado 1',quant_dias=-4)
        cria_enquete(texto='enquete no passado 2',quant_dias=-25)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertQuerysetEqual(resposta.context['ultimas_enquetes'], ['<Pergunta: enquete no passado 1>','<Pergunta: enquete no passado 2>'])

class DetailViewTest(TestCase):
    def test_enquete_no_futuro(self):
        '''
        Está view deverá retornar um codigo 404 para enquetes com data no futuro.
        '''
        enq_futura = cria_enquete("Enquete no futuro",5)
        url = reverse('enquetes:detalhes', args = (enq_futura.id,))
        resposta = self.client.get(url)
        self.assertEqual(resposta.status_code, 404)

    def test_enquete_no_passado(self):
        '''
        Os detalhes das enquetes no passado são exibidas sem problemas
        '''
        enq_passado = cria_enquete("Enquete no passado",-5)
        url = reverse('enquetes:detalhes', args = (enq_passado.id,))
        resposta = self.client.get(url)
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, enq_passado.texto)

