import datetime
from django.db import models
from django.utils import timezone

#### novos modelos de extensão do app#######
############################################

class Rotulo(models.Model):
    titulo = models.CharField(max_length=25)
    def __str__(self):
        return self.titulo

class Perfil(models.Model):
    descricao = models.CharField(max_length=100)
    cidade = models.CharField(max_length=20)
    pais_nacionalidade = models.CharField(max_length=20)
    generos = models.TextField()
    class Meta:
        verbose_name_plural = 'Perfis'

class Autor(models.Model):
    nome = models.CharField(max_length=30)
    idade = models.IntegerField()
    perfil = models.OneToOneField(Perfil, on_delete=models.CASCADE)
    class Meta:
        verbose_name_plural = 'Autores'
    def __str__(self):
        return self.nome


##### primeiros modelos da aplicação######
##########################################
class Pergunta(models.Model):
    texto = models.CharField(max_length = 200)
    data_publicacao = models.DateTimeField('Data de publicação')
    rotulos = models.ManyToManyField(Rotulo)
    autor = models.ForeignKey(Autor, on_delete=models.CASCADE, null = True)
    def __str__(self):
        return self.texto
    def publicada_recentemente(self):
        agora = timezone.now()
        return agora-datetime.timedelta(days=1)<=self.data_publicacao<=agora
    publicada_recentemente.admin_order_field = 'data_publicacao'
    publicada_recentemente.boolean = True
    publicada_recentemente.short_description = 'Foi publicada recentemente?'

class Opcao(models.Model):
    texto = models.CharField(max_length = 100)
    votos = models.IntegerField(default = 0)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Opção'
        verbose_name_plural = 'Opções'

    def __str__(self):
        return self.texto


